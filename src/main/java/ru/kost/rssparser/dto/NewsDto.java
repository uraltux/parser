package ru.kost.rssparser.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewsDto {
    private Long id;
    private String siteUrl;
    private LocalDateTime date;
    private String title;
    private String content;
    private String url;

    public NewsDto(String siteUrl, LocalDateTime date, String title, String content, String url) {
        this.siteUrl = siteUrl;
        this.date = date;
        this.title = title;
        this.content = content;
        this.url = url;
    }
}

