package ru.kost.rssparser.util;

import ru.kost.rssparser.dto.NewsDto;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

public interface Parser {
    List<NewsDto> getListNews(String url) throws IOException;
    LocalDateTime dateConverter(Date date);
}

