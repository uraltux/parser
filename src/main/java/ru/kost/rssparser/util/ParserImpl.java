package ru.kost.rssparser.util;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Component;
import ru.kost.rssparser.dto.NewsDto;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class ParserImpl implements Parser {

    @Override
    public List<NewsDto> getListNews(String url) {
        if (url.endsWith(".rss") || url.endsWith(".xml")) {
            return getNewsByRss(url);
        }
        return getNewsNoRss(url.endsWith("/") ? url.substring(0, url.lastIndexOf("/")) : url);
    }

    public List<NewsDto> getNewsByRss(String url) {
        SyndFeedInput input = new SyndFeedInput();
        SyndFeed feed = null;
        try {
            feed = input.build(new XmlReader(new URL(url)));
        } catch (FeedException | IOException e) {
            System.out.println("Не могу спарсить RSS");
        }
        List<SyndEntry> entries = feed.getEntries();
        List<NewsDto> dtos = new ArrayList<>();
        for (SyndEntry syndEntry : entries) {
            dtos.add(new NewsDto(
                    url
                    , dateConverter(syndEntry.getPublishedDate())
                    , syndEntry.getTitle()
                    , syndEntry.getDescription().getValue()
                    , syndEntry.getLink()));
        }
        return dtos;
    }

    public List<NewsDto> getNewsNoRss(String url) {
        Document doc = null;
        try {
            doc = Jsoup.connect(url)
                    .userAgent("Chrome/4.0.249.0 Safari/532.5")
                    .referrer("http://www.google.com")
                    .get();
        } catch (IOException e) {
            System.out.println("Не могу получить страницу");
        }
        List<NewsDto> list = new ArrayList<>();
        Elements elements = doc.getElementsByAttributeValue("class", "post-card-content ");
        elements.forEach(element -> {
            NewsDto dto = new NewsDto();
            dto.setSiteUrl(url);
            dto.setTitle(element.child(0).child(0).child(1).text());
            dto.setContent(element.child(0).child(1).child(0).text());
            dto.setUrl(url + element.child(0).attr("href"));
            String date = element.child(1).child(1).child(1).child(0).attr("datetime");
            dto.setDate(dateRssConverter(date));
            list.add(dto);
        });
        return list;
    }

    public LocalDateTime dateConverter(Date date) {
        return new Timestamp(date.getTime()).toLocalDateTime();
    }

    public LocalDateTime dateRssConverter(String date) {
        DateTimeFormatter dateformatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate localDate = LocalDate.parse(date, dateformatter);
        return localDate.atStartOfDay();
    }
}
