package ru.kost.rssparser.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ru.kost.rssparser.dao.LastNewsRepository;
import ru.kost.rssparser.dto.NewsDto;
import ru.kost.rssparser.service.NewsService;

import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Configuration
@EnableScheduling
public class CheckingNews {
    @Autowired
    NewsService newsService;
    @Autowired
    Parser parser;
    @Autowired
    LastNewsRepository lastNewsRepository;

    @Scheduled(fixedDelay = 540000)
    public void checkNews() throws ParseException {
        System.out.println("Планировщик запущен");
        List<Object[]> list1 = lastNewsRepository.getLastNews();
        for (Object[] news : list1) {
            LocalDateTime date = parser.dateConverter((Date) news[0]);
            String url = (String) news[1];
            List<NewsDto> newsDtoList = new ArrayList<>();
            try {
                for (NewsDto dto :
                        parser.getListNews(url)) {
                    if (date.isBefore(dto.getDate())) {
                        newsDtoList.add(dto);
                    }
                }
            } catch (IOException e) {
                System.out.println("Планировщик не справился с задачей");
            }
            if (!(newsDtoList.isEmpty())) {
                newsService.saveAllNews(newsDtoList);
            } else System.out.println("Новых новостей нет");
        }
    }
}
