package ru.kost.rssparser.rest;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.kost.rssparser.dto.NewsDto;
import ru.kost.rssparser.service.NewsService;
import ru.kost.rssparser.util.Parser;

import java.io.IOException;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/")
public class ParserRestController {
    @Autowired
    private Parser parser;
    @Autowired
    private NewsService newsService;

    @GetMapping
    public String getHomePage() {
        return "доступные cайт<br>" +
                "Чтобы добавить сайт в базу кликни<br>" +
                "<a href='http://localhost:8080/add?url=https://ria.ru/export/itunes/rss2/istorii_doc.xml'>https://ria.ru/export/itunes/rss2/istorii_doc.xml</a><br>" +
                "<a href='http://localhost:8080/add?url=https://omgubuntu.ru/tag/news/'>https://omgubuntu.ru/tag/news/</a><br>" +
                "<a href='http://localhost:8080/add?url=https://news.yandex.ru/cosmos.rss'>https://news.yandex.ru/cosmos.rss</a><br>" +
                "<br>" +
                "<a href='http://localhost:8080/add?url='> add?url=</a> - добавляет сайт в базу и парсит с него новости<br>" +
                "<a href='http://localhost:8080/news'> news/ </a>- показывает все новости которые есть<br>" +
                "<a href='http://localhost:8080/get/1'>get/{id}</a> - показывает выбранную новость по id (по умолчанию 1)<br>" +
                "<a href='http://localhost:8080/search/linux'>/search/{string}</a> - поиск по заголовкам новости (по умолчанию linux)";
    }

    @GetMapping("add")
    public ResponseEntity<String> addSite(@RequestParam String url) {
        try {
            newsService.saveAllNews(parser.getListNews(url));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok("<a href='http://localhost:8080'>На главную</a>")  ;
    }

    @GetMapping(value = "news", produces = "application/json")
    public ResponseEntity<List<NewsDto>> getAllSiteNews() {
        List<NewsDto> list = newsService.getAllNews();
        return ResponseEntity.ok(list);
    }

    @GetMapping(value = "get/{id}", produces = "application/json")
    public ResponseEntity<NewsDto> getNewsByid(@PathVariable Long id) {
        return ResponseEntity.ok(newsService.getNewsById(id));
    }

    @GetMapping(value = "search/{string}", produces = "application/json")
    public ResponseEntity<List<NewsDto>> searchNews(@PathVariable String string) {
        List<NewsDto> list = newsService.searchByTitle(string);
        return ResponseEntity.ok(list);
    }
}
