package ru.kost.rssparser.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kost.rssparser.dao.NewsRepository;
import ru.kost.rssparser.dto.NewsDto;
import ru.kost.rssparser.model.RssNews;
import ru.kost.rssparser.model.SiteNews;
import ru.kost.rssparser.service.NewsService;

import javax.persistence.EntityExistsException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NewServiceImpl implements NewsService {
    @Autowired
    NewsRepository newsRepository;

    @Override
    public NewsDto getNewsById(Long id) {
        return converterNewsSite(newsRepository.findById(id).orElseThrow(() -> new EntityExistsException("нет такой новости")));
    }

    @Override
    public void saveNews(NewsDto newsDto) {
        newsRepository.save(converter(newsDto));
    }

    @Override
    public SiteNews converter(NewsDto newsDto) {
        SiteNews siteNews = new SiteNews();
        RssNews rssNews = new RssNews();
        if (newsDto.getId() != null) {
            siteNews.setId(newsDto.getId());
        }
        rssNews.setDate(newsDto.getDate());
        rssNews.setTitle(newsDto.getTitle());
        rssNews.setContent(newsDto.getContent());
        rssNews.setUrl(newsDto.getUrl());
        siteNews.setSiteUrl(newsDto.getSiteUrl());
        siteNews.setRssNews(rssNews);
        return siteNews;
    }

    public NewsDto converterNewsSite(SiteNews siteNews) {
        return new NewsDto(
                siteNews.getId()
                , siteNews.getSiteUrl()
                , siteNews.getRssNews().getDate()
                , siteNews.getRssNews().getTitle()
                , siteNews.getRssNews().getContent()
                , siteNews.getRssNews().getUrl()
        );
    }

    @Override
    public List<NewsDto> getAllNews() {
        return newsRepository.findAll().stream().map((t) -> converterNewsSite(t)).collect(Collectors.toList());
    }

    @Override
    public void saveAllNews(List<NewsDto> list) {
        for (NewsDto d :
                list) {
            saveNews(d);
        }
    }

    @Override
    public List<NewsDto> searchByTitle(String s) {
        return newsRepository.findBiLikeTitle(s).stream().map(t -> converterNewsSite(t)).collect(Collectors.toList());
    }
}
