package ru.kost.rssparser.service;

import ru.kost.rssparser.dto.NewsDto;
import ru.kost.rssparser.model.SiteNews;

import java.util.List;

public interface NewsService {
    NewsDto getNewsById(Long id);
    void saveNews(NewsDto newsDto);
    SiteNews converter(NewsDto newsDto);
    List<NewsDto> getAllNews();
    void saveAllNews(List<NewsDto> list);
    List<NewsDto> searchByTitle(String s);


}
