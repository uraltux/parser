package ru.kost.rssparser.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Lob;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class RssNews {
    private LocalDateTime date;
    private String title;
    @Lob
    @Column(length = 512)
    private String content;
    private String url;
}
