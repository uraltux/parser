package ru.kost.rssparser.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kost.rssparser.model.SiteNews;

import java.util.List;

public interface NewsRepository extends JpaRepository<SiteNews, Long> {
    @Query(value = "SELECT * from site_news WHERE sites_id=:q", nativeQuery = true)
    List<SiteNews> getSiteNewsByIdSites(@Param("q") Long id);

    @Query(value = "SELECT * FROM site_news WHERE title like %:title%", nativeQuery = true)
    List<SiteNews> findBiLikeTitle(@Param("title") String title);


}
