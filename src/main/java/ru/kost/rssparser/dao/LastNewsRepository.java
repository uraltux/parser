package ru.kost.rssparser.dao;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class LastNewsRepository {
    @PersistenceContext(type = PersistenceContextType.EXTENDED)
    private EntityManager entityManager;

    public List<Object[]> getLastNews() {
        Query query = entityManager.createNativeQuery(" select max(a.date), site_url from site_news as a group by a.site_url");
        return query.getResultList();
    }
}
